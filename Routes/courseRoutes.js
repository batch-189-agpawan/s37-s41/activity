const express = require('express');
const router = express.Router();
const auth = require('../auth')

const courseController = require('../controllers/courseController');

//Route for creating a course
//This should be at the controller script. make it a habit since routes are just for routes.
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Unauthorized to add course')
	}



});

//Route for retrieving all the courses
router.get("/all", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});

//Route for retrieving all ACTIVE courses
router.get("/", (req,res) =>{

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving a specific course
router.get("/:courseId", (req, res) =>{

	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


//Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


//Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

            const userData = auth.decode(req.headers.authorization);

            if (userData.isAdmin) {
                courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
            } else {

                res.send({ auth: "failed" });
    }

})







module.exports = router;


