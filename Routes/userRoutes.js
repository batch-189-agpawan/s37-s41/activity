const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/details", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);
	// const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	// console.log(isAdminData);
	// console.log(userData);
	// console.log(req.headers.authorization);

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})


// router.post("/enroll", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization)
// 	let data = {
// 		userId: userData.id,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data,).then(resultFromController => res.send(resultFromController))

// });

solution:
router.post("/enroll", auth.verify, (req,res) =>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})




module.exports = router;